package com.test.testapp;

import android.app.Activity;
import android.content.Context;

import com.test.testapp.activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainActivityTest {

    private Context mContext;
    private Activity activity;

    @Before
    public void setUp()  {
        mContext = RuntimeEnvironment.application;
        MockitoAnnotations.initMocks(this);
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
    }

    @Test
    public void isSomeTest() {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        activity.findViewById(R.id.fab).performClick();

        assert true;
    }

    @Test
    public void isListEmptyOnStart() {

    }

    @After
    public void tearDown() throws Exception{

    }
}