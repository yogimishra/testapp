package com.test.testapp.entities;

/**
 * Created by yogeshmishra on 07/01/16.
 */
public class Joke {
    int mJokeId;
    String mJokeDescription;

    public int getJokeId() {
        return mJokeId;
    }

    public void setJokeId(int mJokeId) {
        this.mJokeId = mJokeId;
    }

    public String getJokeDescription() {
        return mJokeDescription;
    }

    public void setJokeDescription(String mJokeDescription) {
        this.mJokeDescription = mJokeDescription;
    }
}
