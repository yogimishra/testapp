package com.test.testapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.testapp.R;
import com.test.testapp.activities.ActivityBase;
import com.test.testapp.entities.Joke;

import java.util.ArrayList;

public class JokesListAdapter extends BaseAdapter {

	ArrayList<Joke> jokes = new ArrayList<Joke>();
	ActivityBase context;
    LayoutInflater mLayoutInflater;

    private static class ViewHolder {
        public TextView text;
        public ImageView image;
    }

    public void addJoke(Joke joke) {
        jokes.add(joke);
        notifyDataSetChanged();
    }
	
	public JokesListAdapter(ActivityBase context) {
		this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = mLayoutInflater.inflate(R.layout.jokes_list_item, parent, false);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.txtJoke);
            holder.image = (ImageView) view.findViewById(R.id.iconJoke);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Joke joke = jokes.get(position);
        holder.text.setText(joke.getJokeDescription());

        return view;
	}
	
	@Override
	public int getCount() {
		return jokes.size();
	}
	
	@Override
	public boolean isEnabled(int position) {
        return true;
    }
	
	@Override
	public Joke getItem(int position) {
		return jokes.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
}
