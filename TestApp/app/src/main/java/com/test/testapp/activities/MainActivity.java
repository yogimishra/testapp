package com.test.testapp.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.test.testapp.R;
import com.test.testapp.adapters.JokesListAdapter;
import com.test.testapp.entities.Joke;
import com.test.testapp.model.CustomVolleyRequestQueue;
import com.test.testapp.requests.CustomJSONObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends ActivityBase implements Response.Listener,
        Response.ErrorListener {

    public static final String REQUEST_TAG = "MainVolleyActivity";

    String url = "http://api.icndb.com/jokes/random";

    ListView mJokesListView;
    JokesListAdapter mJokesListAdapter;

    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mJokesListView = (ListView) findViewById(R.id.jokeslist_view);
        mJokesListAdapter = new JokesListAdapter(this);
        mJokesListView.setAdapter(mJokesListAdapter);

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method
                        .GET, url,
                        new JSONObject(), MainActivity.this, MainActivity.this);
                jsonRequest.setTag(REQUEST_TAG);
                mQueue.add(jsonRequest);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext())
                .getRequestQueue();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mQueue != null) {
            mQueue.cancelAll(REQUEST_TAG);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(Object response) {
        try {

            JSONObject responseObj = (JSONObject) response;
            JSONObject responseObjJoke = (JSONObject) responseObj.get("value");
            int jokeId = responseObjJoke.getInt("id");
            String jokeDescription = responseObjJoke.getString("joke");

            Joke joke = new Joke();
            joke.setJokeId(jokeId);
            joke.setJokeDescription(jokeDescription);
            mJokesListAdapter.addJoke(joke);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
